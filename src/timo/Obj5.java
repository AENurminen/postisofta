/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class Obj5 extends ThirdClass {
    
     String Name;
            
    public Obj5(){
        
         Name="Lipasto ";
         broken=false;
         
         size=200000;
         weight=30;
         
    }

    @Override
    public boolean isBroken() {
        return broken;
    }
    
    public String getName() {
        return Name;
    }
    
}
