/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author Nurminen
 */
public class PacketController implements Initializable {

    @FXML
    private TextField name_textfield;
    @FXML
    private TextField receiver_textfield;
    @FXML
    private TextField size_textfield;
    @FXML
    private TextField mass_textfield;
    @FXML
    private ComboBox<Thing> Things_comboBox;
    @FXML
    private ComboBox<String> CityStart_cb;
    @FXML
    private ComboBox<String> CityStartAuto_cb;
    @FXML
    private ComboBox<String> CityEnd_cb;
    @FXML
    private ComboBox<String> CityEndAuto_cb;

    private ArrayList al = new ArrayList();
    private ArrayList sp;

    @FXML
    private CheckBox broken_checkBox;
    @FXML
    private CheckBox FirstClass_cb;
    @FXML
    private CheckBox SecondClass_cb;
    @FXML
    private CheckBox ThirdClass_cb;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        String name = new Obj1().getName();
        int size = new Obj1().getSize();
        int weight = new Obj1().getWeight();
        boolean broken = new Obj1().isBroken();

        Thing t1 = new Thing(name, size, weight, broken);
        Things_comboBox.getItems().add(t1);

        name = new Obj2().getName();
        size = new Obj2().getSize();
        weight = new Obj2().getWeight();
        broken = new Obj2().isBroken();

        Thing t2 = new Thing(name, size, weight, broken);
        Things_comboBox.getItems().add(t2);

        name = new Obj3().getName();
        size = new Obj3().getSize();
        weight = new Obj3().getWeight();
        broken = new Obj3().isBroken();

        Thing t3 = new Thing(name, size, weight, broken);
        Things_comboBox.getItems().add(t3);

        name = new Obj4().getName();
        size = new Obj4().getSize();
        weight = new Obj4().getWeight();
        broken = new Obj4().isBroken();

        Thing t4 = new Thing(name, size, weight, broken);
        Things_comboBox.getItems().add(t4);

        name = new Obj5().getName();
        size = new Obj5().getSize();
        weight = new Obj5().getWeight();
        broken = new Obj5().isBroken();

        Thing t5 = new Thing(name, size, weight, broken);
        Things_comboBox.getItems().add(t5);

        try {
            SetCombo();
        } catch (IOException | SAXException ex) {
            Logger.getLogger(PacketController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void CreateThing_Action(ActionEvent event) {

        String name = name_textfield.getText();
        int size = Integer.valueOf(size_textfield.getText());
        int weight = Integer.valueOf(mass_textfield.getText());
        boolean broken = broken_checkBox.isSelected();
        System.out.println("Seuraavana broken");
        System.out.println(broken);

        Thing thing = new Thing(name, size, weight, broken);

        Things_comboBox.getItems().add(thing);
    }

    private void SetCombo() throws IOException, SAXException {

        String City;

        City = "Lähtökaupunki";

        System.out.println(City);

        XMLTiedot w = new XMLTiedot(City);

        CityStart_cb.getItems().addAll(w.getCitys());
        CityEnd_cb.getItems().addAll(w.getCitys());
    }

    @FXML
    private void CityStart_cb_Action(ActionEvent event) throws IOException, SAXException {

        String City;

        CityStartAuto_cb.getItems().clear();
        City = CityStart_cb.getValue();

        System.out.println(City);

        XMLTiedot w = new XMLTiedot(City);

        CityStartAuto_cb.getItems().addAll(w.getCitysAuto());
    }

    @FXML
    private void CityEnd_cb_Action(ActionEvent event) throws IOException, SAXException {
       

        String City;

        CityEndAuto_cb.getItems().clear();

        City = CityEnd_cb.getValue();

        System.out.println(City);

        XMLTiedot w = new XMLTiedot(City);

        CityEndAuto_cb.getItems().addAll(w.getCitysAuto());
    }

    @FXML
    private void CreatePacket_Action(ActionEvent event) throws SAXException, IOException {

        String startAuto = CityStartAuto_cb.getValue();
        String endAuto = CityEndAuto_cb.getValue();

        System.out.println("CreatePacket_Action alussa. seuraavaksi startauto ja endauto");
        System.out.println(startAuto);
        System.out.println(endAuto);

        XMLTiedot2 postOfficeKoord = new XMLTiedot2(startAuto);

        Float lat = postOfficeKoord.getLat();
        Float lng = postOfficeKoord.getLng();

        XMLTiedot2 postOfficeKoord2 = new XMLTiedot2(endAuto);

        Float lat2 = postOfficeKoord2.getLat();
        Float lng2 = postOfficeKoord2.getLng();

        String Receiver = receiver_textfield.getText();
        String name = Things_comboBox.valueProperty().getValue().getName();
        int size = Things_comboBox.valueProperty().getValue().getSize();
        int weight = Things_comboBox.valueProperty().getValue().getWeight();
        boolean Broken = Things_comboBox.valueProperty().getValue().isBroken();
        int speed;
        boolean broken;
        
        int Size = 0;
        int Weight = 0;
        int Pclass = 0;

        if (FirstClass_cb.isSelected()) {
            speed = (int) new FirstClass().getSpeed();
            broken = new FirstClass().isBroken();
            Pclass=1;
            
        } else if (SecondClass_cb.isSelected()) {
            speed =  new SecondClass().getSpeed();
            broken = new SecondClass().isBroken();
            Size=new SecondClass().getSize();
            Weight=new SecondClass().getWeight();
        } else {
            speed = new ThirdClass().getSpeed();
            broken = new ThirdClass().isBroken();
        }

        System.out.println(Receiver);
        System.out.println(name);
        System.out.println("size and weight");
        System.out.println(size);
        System.out.println(weight);
        System.out.println("Size and Weight");
        System.out.println(Size);
        System.out.println(Weight);
        System.out.println(startAuto);
        System.out.println(endAuto);
        System.out.println(Broken);
        System.out.println(speed);
        System.out.println(Pclass);

        if (Broken == true && broken == true     ) {
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Särkyvä esine ei sovi valisemaasi pakettiluokkaan!");
            alert.setHeaderText(null);
            alert.setContentText("Valitse uusi pakettiluokka!");
            alert.showAndWait();

        } else if(SecondClass_cb.isSelected()&&size>Size){
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Esineesi koko on liian suuri valisemaasi pakettiluokkaan!");
            alert.setHeaderText(null);
            alert.setContentText("Valitse uusi pakettiluokka!");
            alert.showAndWait();
        }
        
        else if(SecondClass_cb.isSelected()&&weight>Weight){
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Esineesi paino on liian suuri valisemaasi pakettiluokkaan!");
            alert.setHeaderText(null);
            alert.setContentText("Valitse uusi pakettiluokka!");
            alert.showAndWait();
        }
        
        else {

            Packet p = new Packet(Receiver, name, size, weight, lat, lng, lat2, lng2, Broken, speed, Pclass);

            Storage st = new Storage("Storage");
            st.setPackets(al);    // hakee PacketControllerin privaatin ArrayList al:n ja asettaa sen Storageen
            st.addPacket(p);

            Packet test = st.getPacket(0);

            al = st.getPackets();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Paketti tehty onnistuneesti!");
            alert.setHeaderText(null);
            alert.setContentText("Paketti tehty!");
            alert.showAndWait();

        }

    }

    @FXML
    private void Exit_Action(ActionEvent event) throws IOException {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
        Scene home_page_scene = new Scene((Pane) loader.load());
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.setResizable(true);
        app_stage.setMaximized(true);

        FXMLDocumentController controller = loader.<FXMLDocumentController>getController();

        app_stage.show();
        controller.setal(al);


    }

}
