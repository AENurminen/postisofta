/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class SecondClass extends Packet{
    
      

    
    public SecondClass() {
        
            speed=2;
            size=6000;
            weight=5;
            
            
            distance=150;
            broken=false;
         
    }

    

    public int getDistance() {
        return distance;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public boolean isBroken() {
        return broken;
    }

    /**
     *
     * @return
     */
    @Override
    public int getSpeed() {
        return speed;
    }
     
}
