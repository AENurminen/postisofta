/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

/**
 *
 * @author Nurminen
 */
public class ThirdClass  extends Packet{
    
     

    
    public ThirdClass() {
        
            speed=3;
            size=40000;
            weight=10;
            
            distance=150;
            broken=true;
         
    }

    public int getSpeed() {
        return speed;
    }

    public int getDistance() {
        return distance;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isBroken() {
        return broken;
    }
}
