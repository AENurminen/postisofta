/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package timo;

import java.util.ArrayList;

/**
 *
 * @author Nurminen
 */
public class Storage {
    
    private ArrayList<Packet>Packets = new ArrayList();
    private String name;

    public Storage(String n) {
        name = n;
    }
    
    public void addPacket(Packet p){
        Packets.add(p);
    }
    
    public void setPackets(ArrayList al){
        Packets=al;
    }
    
    public Packet getPacket(int i){
        
        
            return Packets.get(i);
        
        
    }
    
    public ArrayList<Packet> getPackets(){
        return Packets;
    }
    
}
